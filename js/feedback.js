/**
 * Created with JetBrains PhpStorm.
 * User: Vitaly
 * Date: 06.06.13
 * Time: 20:22
 * To change this template use File | Settings | File Templates.
 */
function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(typeof haystack[i] == 'object') {
            if(arrayCompare(haystack[i], needle)) return true;
        } else {
            if(haystack[i] == needle) return true;
        }
    }
    return false;
}

window.isset = function (v) {
    if (typeof(v) == 'object' && v == 'undefined') {
        return false;
    } else  if (arguments.length === 0) {
        return false;
    } else {
        var buff = arguments[0];
        for (var i = 0; i < arguments.length; i++){
            if (typeof(buff) === 'undefined' || buff === null) return false;
            buff = buff[arguments[i+1]];
        }
    }
    return true;
}

function myconf() {
    var cf = $.Deferred();
        $.ajax({
            type: 'POST',
            url: 'feedback/',
            dataType: 'json',
            data: 'act=cfg',
            success: function(answer) { 
                cf.resolve(answer.configs); 
            }
        }); 
    return cf;
}

var mcf = myconf();
 
mcf.done(function(conf) { 
  $(document).ready(function() {

    /* Вставка антиспам Inputov*/ 
    (function() {
               var fb = $('.feedback');
               if(fb.length > 0) {
                    fb.each(function(){
                        var form = $(this).closest('form'), 
                            name = form.attr('name'); 

                        if(isset(conf[name]) && isset(conf[name].cfg.antispamjs)) {

                          $(form).prepend('<input type="text" name="'+ conf[name].cfg.antispamjs +'" value="tesby" style="display:none;">');

                        }
                    });
                }
      })();
      /* Вставка антиспам Inputov /*/ 

  });


  /**
   * Отправка форм. 
   */

  function feedback(vars) {

      var bt = $(vars.form).find('.feedback');

      var btc = bt.clone();

      var bvc = bt.val();

      var cfg = conf[vars.act].cfg; 
 

      $.ajax({

          type: 'POST',
          url: 'feedback/',
          cache: false,
          dataType: 'json',
          data: 'act=' + vars.act + '&' + vars.data,

          beforeSend: function() {
             
              $(bt).val('ОТПРАВЛЯЕМ ЗАПРОС').html('ОТПРАВЛЯЕМ ЗАПРОС');
              
              $(bt).prop("disabled", true);
              
              $(bt).addClass('loading');
          },

          success: function(answer) { 

            $(bt).val('ЗАКАЗАТЬ ЗВОНОК').html('ЗАКАЗАТЬ ЗВОНОК');

            if(isset(cfg.notify) && !/none/i.test(cfg.notify)) {

              if(/after/i.test(cfg.notify)) {

                  if(isset(answer.errors)) {
                      

                    $.each(vars.form.find('input:visible'), function(e, v) {

                        var name = $(v).attr('name');

                        /*Имя инпута есть списке ошибок?*/
                        if (name in answer.errors) {
                            /*После инпута не стоит сообщение об ошибке?*/
                            if (!$(v).next('.error__info').length > 0) {

                                $(v)
                                    .after('<div class="error__info" style="display:none;">' +
                                        answer.errors[name] + '</div>')
                                    .next('.error__info').fadeIn(400);

                            }   
                              /*Если  стоит, но ошибка другая*/
                              else if( $(v).next('.error__info').html() !== answer.errors[name] ) {
                              $(v).next('.error__info').html(answer.errors[name]);
                            }



                        } else {

                            $(v).next('.error__info').fadeOut(400, function() {
                              $(this).remove();
                            });


                        }

                    });



                      // $.each(answer.errors, function(k,val) {
                      //   // $('[name="'+k+'"]').next('.error__info').remove()
                      //   $('[name="'+k+'"]').after('<div class="error__info" style="display:none;">'+val+'</div>');
                      //   $('.error__info').fadeIn(100);
                      // });

                  } 

                  if(isset(answer.infos)) {
 
                      $(vars.form)
                        .after('<div class="success" style="display:none;">' + answer.infos[0] + '</div>')
                        .slideUp(400, function() {
                          $(vars.form).next('.success').slideDown(400);
                        });
                  } 
              }   

              if (/textbox/i.test(cfg.notify)) {

                 if (isset(answer.errors)) {
                     $.each(answer.errors, function(k, val) {
                         $.jGrowl(val, { theme: 'error', header: 'Ошибка!', life: 3000 });

                     });

                 }
                 if (isset(answer.infos)) {

                     $.each(answer.infos, function(k, val) {
                         $.jGrowl(val, { theme: 'infos', header: 'Внимание!', life: 3000 });
                     });

                 }
              }

              if(/color/i.test(cfg.notify)) {

                   $(vars.form).find('input[type=text]:visible, textarea:visible, select:visible').addClass('error');

                   if(isset(answer.errors)) {
                       $.each(answer.errors, function(k,val) {
                           var reg = /[a-z]/i;
                           if(reg.test(k)) {
                            var e = $(vars.form).find('[name='+ k +']');
                            if(e.length == 1) {
                             // $(e).css({'border': '1px solid #FF532E'}, 100);
                            }
                          }
                       });
                   } if(isset(answer.infos)) {
                        var li='', $inf = $('<ul>', {id:'feedback-infolist'});
                         $.each(answer.infos, function(k,val) {
                            li += '<li>'+ val +'</li>';
                         });

                        $inf.html(li);

                        $.arcticmodal('close');

                        if(/modal/i.test(cfg.notify)) {
                            var m = $('<div class="box-modal" id="feedback-modal-box" />');
                            m.html($inf);
                            m.prepend('<div class="modal-close arcticmodal-close">X</div>');
                            $.arcticmodal({content: m});
                        }
                         //bt.replaceWith($inf);

                       /* setInterval(function(){
                          //$('#feedback-inf-box').replaceWith(btc);
                          $('#feedback-modal-box').arcticmodal('close');
                        }, 4000);*/
                    }
              }

            }

              $(bt).prop("disabled", false);
              $(bt).removeClass('loading');
              //$(bt).val(bvc);

            if(isset(answer.ok) && answer.ok == 1) {
                $(vars.form)[0].reset();
            }
          }
      });

  }

     // АНТИСПАМ 
    $(document).on('mouseenter mouseover', '.feedback', function(){
        var form = $(this).closest('form'), name = form.attr('name');
        if(isset(conf[name]) && isset(conf[name].cfg.antispamjs)) {
            $('input[name='+ conf[name].cfg.antispamjs +']').val('');
        }
    });
    // АНТИСПАМ /


  /**
   * Обработчик кнопки форм.  
   */ 
  $(document).on('click', '.feedback', function(){
     var form = $(this).closest('form'), 
          name = form.attr('name'), 
          obj = {};
         obj.form = form;
         obj.act = name;
         obj.data = $(form).serialize();

        feedback(obj);

      return false;
  });

}); // done